/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    ADM.initialiseWithDefaultDevices(2, 2);
    ADM.addAudioCallback(this);
    ADM.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    ADM.addMidiInputCallback(String::empty, this);
    
    slider.setSliderStyle(juce::Slider::LinearVertical);
    slider.setBounds(0, 0, getWidth()/7, getHeight()/4);
    slider.setRange(0, 1);
    addAndMakeVisible(slider);

}

MainComponent::~MainComponent()
{
    ADM.removeAudioCallback(this);
    ADM.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{

}

void MainComponent:: audioDeviceIOCallback (const float** inputChannelData,int numInputChannels,float** outputChannelData,int numOutputChannels,int numSamples)
{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float gainSlider = slider.getValue();
    
    while(numSamples--)
    {
        *outL = *inL * gainSlider;
        *outR = *inR * gainSlider;
        inL++;
        inR++;
        outL++;
        outR++;
    }
    
}

void MainComponent::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("Audio Device About To Start");
}

void MainComponent::audioDeviceStopped()
{
    DBG("Audio Device Stopped");
}

void MainComponent::handleIncomingMidiMessage (MidiInput* source,
                                                    const MidiMessage& message)
{
    DBG("Incoming Midi Message");
    
    ADM.getDefaultMidiOutput()->sendMessageNow(message);
    
    midiChannel = message.getChannel();
    
    midiValue = message.getVelocity();
    
    DBG("Ch = " << midiChannel << " V = " << midiValue);
}
